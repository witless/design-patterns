package com.github.prontera.structural.facade;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        new TroubleFacade().boom();
    }
}
