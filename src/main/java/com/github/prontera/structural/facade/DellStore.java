package com.github.prontera.structural.facade;

/**
 * @author Zhao Junjian
 */
public class DellStore {
    public void bankrupt() {
        System.out.println("bankrupt " + this.getClass().getSimpleName());
    }
}
