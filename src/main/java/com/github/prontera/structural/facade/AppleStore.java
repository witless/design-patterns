package com.github.prontera.structural.facade;

/**
 * @author Zhao Junjian
 */
public class AppleStore {
    public void destroy() {
        System.out.println("destroying " + this.getClass().getSimpleName());
    }
}
