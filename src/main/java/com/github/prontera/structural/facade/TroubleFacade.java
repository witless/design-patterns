package com.github.prontera.structural.facade;

/**
 * @author Zhao Junjian
 */
public class TroubleFacade {
    private AppleStore appleStore;
    private DellStore dellStore;

    {
        appleStore = new AppleStore();
        dellStore = new DellStore();
    }

    public void boom() {
        appleStore.destroy();
        dellStore.bankrupt();
    }
}
