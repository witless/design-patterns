package com.github.prontera.structural.proxy;

/**
 * @author Zhao Junjian
 */
public interface Laptop {
    String start();

    String shutdown();
}
