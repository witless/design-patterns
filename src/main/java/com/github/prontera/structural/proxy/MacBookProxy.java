package com.github.prontera.structural.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Zhao Junjian
 */
public class MacBookProxy implements InvocationHandler {
    private Laptop target;

    public Laptop getProxy(MacBook macBook) {
        this.target = macBook;
        return (Laptop) Proxy.newProxyInstance(
                macBook.getClass().getClassLoader(),
                macBook.getClass().getInterfaces(),
                this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("dynamic proxy");
        return method.invoke(target, args);
    }
}
