package com.github.prontera.structural.proxy;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final MacBook macBook = new MacBook();
        final Laptop proxy = new MacBookProxy().getProxy(macBook);
        proxy.shutdown();
    }
}
