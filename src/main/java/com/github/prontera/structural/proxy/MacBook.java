package com.github.prontera.structural.proxy;

/**
 * @author Zhao Junjian
 */
public class MacBook implements Laptop {
    @Override
    public String start() {
        System.out.println("start...");
        return "running";
    }

    @Override
    public String shutdown() {
        System.out.println("boom !");
        return "boom!";
    }

}
