package com.github.prontera.structural.decorator;

/**
 * @author Zhao Junjian
 */
public class MacBook implements Laptop {
    @Override
    public String feature() {
        return "MacBook Pro";
    }
}
