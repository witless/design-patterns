package com.github.prontera.structural.decorator;

/**
 * @author Zhao Junjian
 */
public class TouchBarDecorator extends MacBookDecorator {
    protected TouchBarDecorator(Laptop laptop) {
        super(laptop);
    }

    @Override
    public String feature() {
        return "Touch Bar " + this.laptop.feature();
    }
}
