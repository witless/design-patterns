package com.github.prontera.structural.decorator;

/**
 * @author Zhao Junjian
 */
public interface Laptop {
    String feature();
}
