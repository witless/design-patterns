package com.github.prontera.structural.decorator;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final MacBook macBook = new MacBook();
        final TouchBarDecorator greyMacBook = new TouchBarDecorator(macBook);
        final SpaceGreyDecorator touchBar = new SpaceGreyDecorator(greyMacBook);
        System.out.println(touchBar.feature());
    }
}
