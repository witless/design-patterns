package com.github.prontera.structural.decorator;

/**
 * @author Zhao Junjian
 */
public abstract class MacBookDecorator implements Laptop {
    protected Laptop laptop;

    protected MacBookDecorator(Laptop laptop) {
        this.laptop = laptop;
    }

    @Override
    public abstract String feature();
}
