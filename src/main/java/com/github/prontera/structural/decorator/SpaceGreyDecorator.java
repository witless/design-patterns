package com.github.prontera.structural.decorator;

/**
 * @author Zhao Junjian
 */
public class SpaceGreyDecorator extends MacBookDecorator {
    protected SpaceGreyDecorator(Laptop laptop) {
        super(laptop);
    }

    @Override
    public String feature() {
        return "Space Grey " + this.laptop.feature();
    }
}
