package com.github.prontera.structural.adaptor;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        // 我们有一台普通的安卓手机
        final AndroidPhone androidPhone = new AndroidPhone();
        // 被奸商拿去改成iPhone骗小白
        final IPhoneAdaptor iPhone = new IPhoneAdaptor(androidPhone);
        // 视频通话时玛德黑屏
        iPhone.videoCall();
    }
}
