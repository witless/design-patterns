package com.github.prontera.structural.adaptor;

/**
 * @author Zhao Junjian
 */
public interface IPhone {
    void videoCall();
}
