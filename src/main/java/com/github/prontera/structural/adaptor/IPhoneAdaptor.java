package com.github.prontera.structural.adaptor;

/**
 * @author Zhao Junjian
 */
public class IPhoneAdaptor implements IPhone {
    private AndroidPhone androidPhone;

    public IPhoneAdaptor(AndroidPhone androidPhone) {
        this.androidPhone = androidPhone;
    }

    @Override
    public void videoCall() {
        System.out.println("无法抗拒的黑屏");
        androidPhone.call();
    }
}
