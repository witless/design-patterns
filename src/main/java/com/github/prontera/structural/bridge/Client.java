package com.github.prontera.structural.bridge;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final MessageService messageService = new EmailMessageService();
        final UserService userService = new ChineseService(messageService);
        userService.giveBirth();
    }
}
