package com.github.prontera.structural.bridge;

/**
 * @author Zhao Junjian
 */
public abstract class UserService {
    protected MessageService messageService;

    protected UserService(MessageService messageService) {
        this.messageService = messageService;
    }

    public abstract void giveBirth();
}
