package com.github.prontera.structural.bridge;

/**
 * @author Zhao Junjian
 */
public class SmsMessageService implements MessageService {
    @Override
    public void send() {
        System.out.println("sms");
    }
}
