package com.github.prontera.structural.bridge;

/**
 * @author Zhao Junjian
 */
public interface MessageService {
    void send();
}
