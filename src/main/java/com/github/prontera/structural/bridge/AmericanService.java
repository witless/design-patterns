package com.github.prontera.structural.bridge;

/**
 * @author Zhao Junjian
 */
public class AmericanService extends UserService {
    protected AmericanService(MessageService messageService) {
        super(messageService);
    }

    @Override
    public void giveBirth() {
        System.out.println("========");
        System.out.println("the American");
        messageService.send();
        System.out.println("========");
    }
}
