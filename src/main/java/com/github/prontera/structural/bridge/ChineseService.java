package com.github.prontera.structural.bridge;

/**
 * @author Zhao Junjian
 */
public class ChineseService extends UserService {

    protected ChineseService(MessageService messageService) {
        super(messageService);
    }

    @Override
    public void giveBirth() {
        System.out.println("========");
        System.out.println("the Asian");
        messageService.send();
        System.out.println("========");
    }
}
