package com.github.prontera.behavioral.handler;

import java.util.Objects;

/**
 * @author Zhao Junjian
 */
public class CtoHandler extends Handler {
    @Override
    public String handleRequest(String whom) {
        if (Objects.equals("cto", whom)) {
            return this.getClass().getSimpleName();
        }
        if (successor != null) {
            return successor.handleRequest(whom);
        }
        throw new IllegalStateException("nobody gonna handler '" + whom + "'");
    }
}
