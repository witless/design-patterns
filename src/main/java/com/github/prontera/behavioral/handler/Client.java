package com.github.prontera.behavioral.handler;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final CtoHandler cto = new CtoHandler();
        final CeoHandler ceo = new CeoHandler();
        final NobodyHandler nobody = new NobodyHandler();
        cto.setSuccessor(ceo);
        ceo.setSuccessor(nobody);
        System.out.println(cto.handleRequest("ceo"));
    }
}
