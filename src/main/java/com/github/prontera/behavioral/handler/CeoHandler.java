package com.github.prontera.behavioral.handler;

import java.util.Objects;

/**
 * @author Zhao Junjian
 */
public class CeoHandler extends Handler {

    @Override
    public String handleRequest(String whom) {
        if (Objects.equals("ceo", whom)) {
            return this.getClass().getSimpleName();
        }
        if (successor != null) {
            return successor.handleRequest(whom);
        }
        throw new IllegalStateException("nobody gonna handler '" + whom + "'");
    }
}
