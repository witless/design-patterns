package com.github.prontera.behavioral.handler;

/**
 * @author Zhao Junjian
 */
public abstract class Handler {
    protected Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    public abstract String handleRequest(String whom);
}
