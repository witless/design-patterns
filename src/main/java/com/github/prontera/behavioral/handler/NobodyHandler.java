package com.github.prontera.behavioral.handler;

/**
 * @author Zhao Junjian
 */
public class NobodyHandler extends Handler {
    @Override
    public String handleRequest(String whom) {
        return "nobody";
    }
}
