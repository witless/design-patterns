package com.github.prontera.behavioral.visitor;

/**
 * @author Zhao Junjian
 */
public class TouchBarVisitor implements Visitor {
    @Override
    public void visit(Laptop laptop) {
        // 可能会调用一下Laptop的方法
        System.out.println(laptop.getClass().getSimpleName() + " got touch bar now!");
    }
}
