package com.github.prontera.behavioral.visitor;

/**
 * @author Zhao Junjian
 */
public class MacBook extends Laptop {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
