package com.github.prontera.behavioral.visitor;

/**
 * @author Zhao Junjian
 */
public abstract class Laptop {

    public abstract void accept(Visitor visitor);

}
