package com.github.prontera.behavioral.visitor;

/**
 * @author Zhao Junjian
 */
public interface Visitor {
    void visit(Laptop laptop);
}
