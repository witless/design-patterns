package com.github.prontera.behavioral.visitor;

/**
 * @author Zhao Junjian
 */
public class Xps13 extends Laptop {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
