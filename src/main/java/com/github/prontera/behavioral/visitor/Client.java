package com.github.prontera.behavioral.visitor;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final TouchBarVisitor visitor = new TouchBarVisitor();
        final Xps13 xps13 = new Xps13();
        final MacBook macBook = new MacBook();
        xps13.accept(visitor);
        macBook.accept(visitor);
    }
}
