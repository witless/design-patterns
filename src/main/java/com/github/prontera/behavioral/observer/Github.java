package com.github.prontera.behavioral.observer;

import java.util.Observable;

/**
 * @author Zhao Junjian
 */
public class Github extends Observable {
    public void newRepo(String repoName) {
        setChanged();
        notifyObservers(repoName);
    }
}
