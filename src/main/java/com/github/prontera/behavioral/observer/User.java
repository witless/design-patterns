package com.github.prontera.behavioral.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * @author Zhao Junjian
 */
public class User implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("observable class: " + o.getClass().getSimpleName());
        System.out.println("content: " + arg);
    }
}
