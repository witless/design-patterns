package com.github.prontera.behavioral.observer;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final User user = new User();
        final Github github = new Github();
        github.addObserver(user);
        github.newRepo("https://github.com/prontera/spring-cloud-rest-tcc");
    }
}
