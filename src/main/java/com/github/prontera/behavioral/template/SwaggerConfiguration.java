package com.github.prontera.behavioral.template;

/**
 * @author Zhao Junjian
 */
public class SwaggerConfiguration extends ConfigurationTemplate {
    @Override
    protected void configureBean() {
        System.out.println(this.getClass().getSimpleName() + " configure bean");
    }

    @Override
    protected void configureInterceptor() {
        System.out.println(this.getClass().getSimpleName() + " configure interceptor");
    }
}
