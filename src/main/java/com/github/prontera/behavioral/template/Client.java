package com.github.prontera.behavioral.template;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final SwaggerConfiguration configuration = new SwaggerConfiguration();
        configuration.afterProperties();
    }
}
