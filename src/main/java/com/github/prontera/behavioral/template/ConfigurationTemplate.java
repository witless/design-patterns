package com.github.prontera.behavioral.template;

/**
 * @author Zhao Junjian
 */
public abstract class ConfigurationTemplate {
    protected abstract void configureBean();

    protected abstract void configureInterceptor();

    public void afterProperties() {
        configureBean();
        configureInterceptor();
        System.out.println("configure all!");
    }
}
