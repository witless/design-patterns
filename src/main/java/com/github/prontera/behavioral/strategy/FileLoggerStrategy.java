package com.github.prontera.behavioral.strategy;

/**
 * @author Zhao Junjian
 */
public class FileLoggerStrategy implements LoggerStrategy {
    @Override
    public void log() {
        System.out.println(this.getClass().getSimpleName());
    }
}
