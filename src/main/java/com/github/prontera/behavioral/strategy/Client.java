package com.github.prontera.behavioral.strategy;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final SqlLoggerStrategy logger = new SqlLoggerStrategy();
        final Context context = new Context(logger);
        context.someBusiness();
    }
}

