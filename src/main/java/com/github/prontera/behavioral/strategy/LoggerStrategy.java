package com.github.prontera.behavioral.strategy;

/**
 * @author Zhao Junjian
 */
public interface LoggerStrategy {
    void log();
}
