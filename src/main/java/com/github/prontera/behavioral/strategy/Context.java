package com.github.prontera.behavioral.strategy;

/**
 * @author Zhao Junjian
 */
public class Context {
    private LoggerStrategy strategy;

    public Context(LoggerStrategy strategy) {
        this.strategy = strategy;
    }

    public void someBusiness() {
        // ...
        System.out.println("busy");
        strategy.log();
    }
}
