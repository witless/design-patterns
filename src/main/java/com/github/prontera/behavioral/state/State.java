package com.github.prontera.behavioral.state;

/**
 * @author Zhao Junjian
 */
public interface State {
    void doSomeWork();
}
