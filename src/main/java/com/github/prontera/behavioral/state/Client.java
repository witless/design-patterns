package com.github.prontera.behavioral.state;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final Context context = new Context();
        context.login();
        context.login();
        context.login();
        context.login();
        context.login();
    }
}
