package com.github.prontera.behavioral.state;

/**
 * @author Zhao Junjian
 */
public class UserLockedState implements State {
    @Override
    public void doSomeWork() {
        System.out.println(this.getClass().getSimpleName());
    }
}
