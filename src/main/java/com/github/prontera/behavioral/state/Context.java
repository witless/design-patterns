package com.github.prontera.behavioral.state;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Zhao Junjian
 */
public class Context {
    private AtomicInteger counter = new AtomicInteger(0);

    public void login() {
        State state = new UserActiveState();
        if (counter.getAndIncrement() > 2) {
            state = new UserLockedState();
        }
        state.doSomeWork();
    }
}
