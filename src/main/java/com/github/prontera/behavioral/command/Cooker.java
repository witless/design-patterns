package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public interface Cooker {
    void cook(String name);
}
