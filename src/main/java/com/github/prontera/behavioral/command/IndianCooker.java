package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public class IndianCooker implements Cooker {
    @Override
    public void cook(String name) {
        System.out.println(this.getClass().getSimpleName() + " is cooking " + name);
    }
}
