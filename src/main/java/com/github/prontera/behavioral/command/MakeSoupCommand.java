package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public class MakeSoupCommand implements MakeFoodCommand {
    private Cooker cooker;

    public MakeSoupCommand(Cooker cooker) {
        this.cooker = cooker;
    }

    @Override
    public void execute() {
        cooker.cook(" soup ");
    }
}
