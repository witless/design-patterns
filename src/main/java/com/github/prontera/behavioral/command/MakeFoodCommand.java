package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public interface MakeFoodCommand {
    void execute();
}
