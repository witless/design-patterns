package com.github.prontera.behavioral.command;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Zhao Junjian
 */
public class BatchMakeCommand implements MakeFoodCommand {
    private Set<MakeFoodCommand> commands = new LinkedHashSet<>();

    public void addCommand(MakeFoodCommand command) {
        commands.add(command);
    }

    @Override
    public void execute() {
        for (MakeFoodCommand command : commands) {
            command.execute();
        }
    }
}
