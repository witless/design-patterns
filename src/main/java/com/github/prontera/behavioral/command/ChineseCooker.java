package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public class ChineseCooker implements Cooker {
    @Override
    public void cook(String name) {
        System.out.println(this.getClass().getSimpleName() + " is cooking " + name);
    }
}
