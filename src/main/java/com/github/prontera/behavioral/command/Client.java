package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final BatchMakeCommand batch = new BatchMakeCommand();
        final MakeSoupCommand makeSoupCommand = new MakeSoupCommand(new IndianCooker());
        batch.addCommand(makeSoupCommand);
        final MakeSeafoodCommand makeSeafoodCommand = new MakeSeafoodCommand(new ChineseCooker());
        batch.addCommand(makeSeafoodCommand);
        batch.execute();
    }
}
