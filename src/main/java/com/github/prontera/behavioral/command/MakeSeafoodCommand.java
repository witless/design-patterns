package com.github.prontera.behavioral.command;

/**
 * @author Zhao Junjian
 */
public class MakeSeafoodCommand implements MakeFoodCommand {
    private Cooker cooker;

    public MakeSeafoodCommand(Cooker cooker) {
        this.cooker = cooker;
    }

    @Override
    public void execute() {
        cooker.cook(" seafood ");
    }
}
