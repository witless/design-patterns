package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public class QualityAssuranceEngineer extends Engineer {
    public QualityAssuranceEngineer(Mediator mediator) {
        super(mediator);
    }

    public void test() {
        System.out.println(this.getClass().getSimpleName());
        getMediator().communicate(this);
    }
}
