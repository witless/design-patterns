package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        // 产品经理
        final ProductManager mediator = new ProductManager();
        // 后端的同事
        final BackendEngineer backendEngineer = new BackendEngineer(mediator);
        // 前端的同事
        final FrontendEngineer frontendEngineer = new FrontendEngineer(mediator);
        // QA的同事
        final QualityAssuranceEngineer qa = new QualityAssuranceEngineer(mediator);
        mediator.setAngular(frontendEngineer);
        mediator.setJava(backendEngineer);
        mediator.setQa(qa);
        // 后端的同事开始工作
        backendEngineer.coding();
    }
}
