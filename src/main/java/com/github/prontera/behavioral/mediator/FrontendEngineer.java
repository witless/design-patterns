package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public class FrontendEngineer extends Engineer {
    public FrontendEngineer(Mediator mediator) {
        super(mediator);
    }

    public void coding() {
        System.out.println(this.getClass().getSimpleName());
        getMediator().communicate(this);
    }
}
