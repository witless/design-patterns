package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public interface Mediator {
    void communicate(Engineer engineer);
}
