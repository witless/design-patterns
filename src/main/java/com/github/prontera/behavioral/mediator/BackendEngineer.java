package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public class BackendEngineer extends Engineer {
    public BackendEngineer(Mediator mediator) {
        super(mediator);
    }

    public void coding() {
        System.out.println(this.getClass().getSimpleName());
        getMediator().communicate(this);
    }
}
