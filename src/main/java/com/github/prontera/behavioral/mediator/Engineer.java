package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public abstract class Engineer {
    protected Mediator mediator;

    public Engineer(Mediator mediator) {
        this.mediator = mediator;
    }

    public Mediator getMediator() {
        return mediator;
    }
}
