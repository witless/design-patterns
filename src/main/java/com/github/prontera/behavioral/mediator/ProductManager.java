package com.github.prontera.behavioral.mediator;

/**
 * @author Zhao Junjian
 */
public class ProductManager implements Mediator {
    private QualityAssuranceEngineer qa;

    private BackendEngineer java;

    private FrontendEngineer angular;

    public void setQa(QualityAssuranceEngineer qa) {
        this.qa = qa;
    }

    public void setJava(BackendEngineer java) {
        this.java = java;
    }

    public void setAngular(FrontendEngineer angular) {
        this.angular = angular;
    }

    @Override
    public void communicate(Engineer engineer) {
        final Class<? extends Engineer> sourceClass = engineer.getClass();
        if (QualityAssuranceEngineer.class.isAssignableFrom(sourceClass)) {
            // 到qa部门就说明已经执行完
            System.out.println("test done!");
        } else if (BackendEngineer.class.isAssignableFrom(sourceClass)) {
            System.out.println("java done, web now");
            angular.coding();
        } else if (FrontendEngineer.class.isAssignableFrom(sourceClass)) {
            System.out.println("web done, qa now");
            qa.test();
        } else {
            throw new IllegalStateException("none");
        }
    }

}
