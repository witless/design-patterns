package com.github.prontera.behavioral.iterator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Zhao Junjian
 */
public class Laptop implements Iterator<Integer> {
    private int[] cores;
    private AtomicInteger index = new AtomicInteger(0);

    public Laptop(int[] cores) {
        this.cores = Arrays.copyOf(cores, cores.length);
    }

    @Override
    public boolean hasNext() {
        return cores != null && index.get() < cores.length;
    }

    @Override
    public Integer next() {
        final int nowIndex = index.getAndAdd(1);
        return cores[nowIndex];
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }

}
