package com.github.prontera.behavioral.iterator;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final Laptop laptop = new Laptop(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0});
        while (laptop.hasNext()) {
            System.out.println(laptop.next());
        }
    }
}
