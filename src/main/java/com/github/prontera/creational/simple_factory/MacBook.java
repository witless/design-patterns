package com.github.prontera.creational.simple_factory;

/**
 * @author Zhao Junjian
 */
public class MacBook implements Laptop {
    @Override
    public void doSomething() {
        System.out.println("rmbp");
    }
}
