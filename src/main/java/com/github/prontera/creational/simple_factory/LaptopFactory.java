package com.github.prontera.creational.simple_factory;

/**
 * @author Zhao Junjian
 */
public class LaptopFactory {

    private LaptopFactory() {

    }

    public static MacBook createMacBook() {
        return new MacBook();
    }

    public static Xps13 createXps13() {
        return new Xps13();
    }

}
