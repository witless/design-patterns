package com.github.prontera.creational.simple_factory;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        // 生成rmbp
        final MacBook implA = LaptopFactory.createMacBook();
        implA.doSomething();
        // 生产xps
        final Xps13 implB = LaptopFactory.createXps13();
        implB.doSomething();
    }
}
