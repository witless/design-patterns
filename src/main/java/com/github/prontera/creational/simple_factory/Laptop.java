package com.github.prontera.creational.simple_factory;

/**
 * @author Zhao Junjian
 */
public interface Laptop {
    void doSomething();
}
