package com.github.prontera.creational.simple_factory;

/**
 * @author Zhao Junjian
 */
public class Xps13 implements Laptop {
    @Override
    public void doSomething() {
        System.out.println("xps13");
    }
}
