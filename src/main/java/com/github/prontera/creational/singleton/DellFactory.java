package com.github.prontera.creational.singleton;

/**
 * @author Zhao Junjian
 */
public class DellFactory {

    private DellFactory() {
        System.out.println("dell is the best");
    }

    private static final class DellFactoryHolder {
        private static final DellFactory FACTORY = new DellFactory();
    }

    public static DellFactory getInstance() {
        return DellFactoryHolder.FACTORY;
    }

    /**
     * 期望调用此方法时可以获得 一个引用指向了刚分配的内存地址 但是却未被初始化的对象
     */
    public Object doSomething() {
        return new Object();
    }
}
