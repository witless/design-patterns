package com.github.prontera.creational.singleton;

/**
 * @author Zhao Junjian
 */
public class AppleFactory {
    private static AppleFactory FACTORY;

    public static AppleFactory getInstance() {
        if (FACTORY == null) {
            synchronized (AppleFactory.class) {
                if (FACTORY == null) {
                    try {
                        ClientTester.LATCH.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    FACTORY = new AppleFactory();
                }
            }
        }
        return FACTORY;
    }

    private AppleFactory() {
        System.out.println(Thread.currentThread().getName() + " wanna get a hurt");
    }

    /**
     * 期望调用此方法时可以获得 一个引用指向了刚分配的内存地址 但是却未被初始化的对象
     */
    public Object doSomething() {
        return new Object();
    }
}
