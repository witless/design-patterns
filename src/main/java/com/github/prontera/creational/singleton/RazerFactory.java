package com.github.prontera.creational.singleton;

/**
 * @author Zhao Junjian
 */
public enum RazerFactory {
    STEALTH;

    public Object doSomething() {
        return new Object();
    }
}
