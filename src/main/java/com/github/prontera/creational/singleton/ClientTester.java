package com.github.prontera.creational.singleton;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Zhao Junjian
 */
public class ClientTester {
    public static final int THREAD_POOL_SIZE = 180;
    public static final CountDownLatch LATCH = new CountDownLatch(THREAD_POOL_SIZE);

    @Test
    public void testConcurrent() throws Exception {
        ExecutorService pool = null;
        try {
            pool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
            for (int i = 0; i < THREAD_POOL_SIZE; i++) {
                pool.execute(() -> {
                    LATCH.countDown();
                    // 期望抛出异常
                    //AppleFactory.getInstance().doSomething();
                    //DellFactory.getInstance().doSomething();
                    RazerFactory.STEALTH.doSomething();
                });
            }
        } finally {
            if (pool != null) {
                pool.shutdown();
            }
        }
    }
}
