package com.github.prontera.creational.prototype;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author Zhao Junjian
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MacBook implements Cloneable {
    private List<Cpu> cpuList;

    private Cpu[] cpuArray;

    private String memory;

    private int keyboard;

    /**
     * @author Zhao Junjian
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    public static class Cpu {
        private int core;
    }

    @Override
    protected MacBook clone() throws CloneNotSupportedException {
        return (MacBook) super.clone();
    }
}
