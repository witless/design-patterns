package com.github.prontera.creational.prototype;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhao Junjian
 */
public class PrototypeTester {
    @Test
    public void testClone() throws Exception {
        // 你就只管我们生成一个List对象就行
        final List<MacBook.Cpu> cpuList = new ArrayList<MacBook.Cpu>();
        for (int i = 1; i <= 4; i++) {
            final MacBook.Cpu cpu = new MacBook.Cpu(i);
            cpuList.add(cpu);
        }
        final MacBook macBook = new MacBook();
        macBook.setCpuArray(cpuList.toArray(new MacBook.Cpu[cpuList.size()]));
        macBook.setCpuList(cpuList);
        macBook.setKeyboard(87);
        macBook.setMemory("16G");
        // 全部准备完成, 我们就开始克隆一个新的实体
        final MacBook newMacBook = macBook.clone();
        Assert.assertEquals(macBook, newMacBook);
        // 修改引用类型
        final MacBook.Cpu newCpu = new MacBook.Cpu(5);
        cpuList.add(newCpu);
        newMacBook.setCpuList(cpuList);
        // 如果这里抛出异常, 就证明是浅拷贝. 实际情况就是浅拷贝.
        Assert.assertNotEquals(macBook, newMacBook);
    }
}
