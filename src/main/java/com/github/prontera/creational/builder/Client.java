package com.github.prontera.creational.builder;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final MacBook macBook = MacBook.builder().processor("core").memory(9481).capacity(102931L).build();
    }
}
