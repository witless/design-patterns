package com.github.prontera.creational.builder;

import lombok.Data;

/**
 * @author Zhao Junjian
 */
@Data
public class MacBook {
    private String processor;

    private int memory;

    private long capacity;

    private String keyboard;

    private MacBook(Builder builder) {
        processor = builder.processor;
        memory = builder.memory;
        capacity = builder.capacity;
        keyboard = builder.keyboard;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String processor;
        private int memory;
        private long capacity;
        private String keyboard;

        public Builder processor(String val) {
            processor = val;
            return this;
        }

        public Builder memory(int val) {
            memory = val;
            return this;
        }

        public Builder capacity(long val) {
            capacity = val;
            return this;
        }

        public Builder keyboard(String val) {
            keyboard = val;
            return this;
        }

        public MacBook build() {
            return new MacBook(this);
        }

    }

}
