package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public interface LaptopFactory {
    Laptop createLaptop();

    PowerAdaptor createPowerAdaptor();
}
