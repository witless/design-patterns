package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class ApplePowerAdaptor implements PowerAdaptor {
    public ApplePowerAdaptor() {
        System.out.println(this.getClass().getSimpleName());
    }
}
