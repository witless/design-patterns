package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class DellPowerAdaptor implements PowerAdaptor {
    public DellPowerAdaptor() {
        System.out.println(this.getClass().getSimpleName());
    }
}
