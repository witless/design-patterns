package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class Xps13 implements Laptop {
    public Xps13() {
        System.out.println(this.getClass().getSimpleName());
    }
}
