package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class DellFactory implements LaptopFactory {
    @Override
    public Laptop createLaptop() {
        return new Xps13();
    }

    @Override
    public PowerAdaptor createPowerAdaptor() {
        return new DellPowerAdaptor();
    }
}
