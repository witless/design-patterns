package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class AppleFactory implements LaptopFactory {
    @Override
    public Laptop createLaptop() {
        return new MacBook();
    }

    @Override
    public PowerAdaptor createPowerAdaptor() {
        return new ApplePowerAdaptor();
    }
}
