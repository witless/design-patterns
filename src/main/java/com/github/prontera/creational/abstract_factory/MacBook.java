package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class MacBook implements Laptop {
    public MacBook() {
        System.out.println(this.getClass().getSimpleName());
    }
}
