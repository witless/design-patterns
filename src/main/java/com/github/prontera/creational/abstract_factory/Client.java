package com.github.prontera.creational.abstract_factory;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final LaptopFactory factory = new AppleFactory();
        factory.createLaptop();
        factory.createPowerAdaptor();
    }
}
