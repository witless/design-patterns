package com.github.prontera.creational.factory_method;

/**
 * @author Zhao Junjian
 */
public class DellFactory extends LaptopFactory {
    @Override
    protected Laptop createLaptop() {
        return new Xps13();
    }
}
