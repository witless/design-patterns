package com.github.prontera.creational.factory_method;

/**
 * @author Zhao Junjian
 */
public class Client {
    public static void main(String[] args) {
        final LaptopFactory laptopFactory = new AppleFactory();
        laptopFactory.someWork();
    }
}
