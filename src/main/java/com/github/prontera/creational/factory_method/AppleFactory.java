package com.github.prontera.creational.factory_method;

/**
 * @author Zhao Junjian
 */
public class AppleFactory extends LaptopFactory {
    @Override
    protected Laptop createLaptop() {
        return new MacBook();
    }
}
