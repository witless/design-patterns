package com.github.prontera.creational.factory_method;

/**
 * @author Zhao Junjian
 */
public class MacBook implements Laptop {

    @Override
    public void shout() {
        System.out.println("rmbp");
    }

}
