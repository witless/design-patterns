package com.github.prontera.creational.factory_method;

/**
 * 注意我们使用的是抽象类
 *
 * @author Zhao Junjian
 */
public abstract class LaptopFactory {
    public void someWork() {
        // .. 主要处理其他业务
        System.out.println("someWork..");
        //将生成推迟到子类
        final Laptop laptop = createLaptop();
        laptop.shout();
    }

    protected abstract Laptop createLaptop();
}
