package com.github.prontera.creational.factory_method;

/**
 * @author Zhao Junjian
 */
public interface Laptop {
    void shout();
}
