package com.github.prontera.creational.factory_method;

/**
 * @author Zhao Junjian
 */
public class Xps13 implements Laptop {
    @Override
    public void shout() {
        System.out.println("xps");
    }
}
